# Commands

Make a new post `$ hugo new posts/title.md`

Start the hugo server (with drafts) `$ hugo serve -D`

Generate the public site `$ hugo`

Push your site on dev branch of Gitlab
    `Git pull`
    `Git add Git`
    `Git commit -m "message"`
    `Git push`

Push your site on master branch of GitLab
`git checkout master && git pull && git merge dev && git push && git checkout dev`
`:q`

# Content & Markdown

## Make a new post
Create/duplicate a new files.md in the content > posts folder

## Write in Markdown synthax
[markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
[markdown](https://www.markdownguide.org/basic-syntax)

## Import images
put images in the content>images folder
and add one of the link below in your post

### full screen images
{{< img src="image-001.jpg" title="add tittle" class="width-full" >}}

### content size images
{{< img src="image-001.JPG" title="add tittle" class="width-content" >}}

### small images
{{< img src="image-001.jpg" title="add tittle" class="width-small" >}}

## website Links
[Text](http://links.com)

## ref to older post
[post]({{< relref "AAA-MM-JJ-post-folder-name" >}})

## board
title  | title  | title
-------|--------|-------
x      | y      | z
x      | y      | z
x      | y      | z
x      | y      | z
x      | y      | z

## insert arduino code (or other, list avaible here: https://gohugo.io/content-management/syntax-highlighting/#list-of-chroma-highlighting-languages)
{{< highlight arduino >}}
the code
{{< /highlight >}}

## insert comment or personal note into the code
cmd+shif+/

## change the code theme
config > default > config.toml
`pygmentsStyle = "default"`
https://help.farbox.com/pygments.html

## change the code CSS
themes > dok > assets > css > components > code.scss
