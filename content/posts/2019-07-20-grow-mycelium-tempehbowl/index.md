---
projects: ["Grow Mycelium"]
title:  "Tempeh bowl"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

→ See my [Introduction about create a composite material with Mycelium and Straw]({{< relref "2019-04-02-mycelium-straw-intro" >}}) to learn my interest for mycelium as a new bio-material.

## Tempeh

Tempeh is a traditional Indonesian soy product, that is made from fermented soybeans. It is made by a natural culturing and controlled fermentation process that binds soybeans into a cake form. A special fungus is used, which has the Latin name Rhizopus oligosporus, usually marketed under the name Tempeh starter, and develop mycelium between the soybeans to create a compact block.

### Recipe

- soak ~250gr of soja beans during 12 hours
- dry the soja beans and put them in a big bowl
- put a little bit of tempeh starter (a quarter of a teaspoon is more than enough) and mix well
- put everything in a plastic bag (like freezing bags), give a shape, and do lines of holes in the all package with the end of a knife
- put the bag in an incubator, in your boiler room or in your oven with the light on for 24h-32h
- when you have a compact block, it's ready! You can conserve it in the fridge and cook it in a pan before eating

## First shape try

### Tempeh bowl

{{< img src="mycelium-vrac01.jpg" title="" class="width-small" >}}
{{< img src="mycelium-vrac02.jpg" title="" class="width-small" >}}

Before the fermentation process, I put the preparation in a bowl against the edges and I put a smaller bowl above. I wrapped it up with plastic film and made holes with a knife.

As you can see on the picture, the mycelium didn't grow in the bottom of the bowl because because it didn't have enough air.
