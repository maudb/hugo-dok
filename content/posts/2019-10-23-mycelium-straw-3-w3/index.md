---
projects: ["Mycelium, straw and weaving"]
title:  "Week 3"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-3-week3-01.jpg" title="" class="width-content" >}}
{{< img src="mycelium-straw-3-week3-02.jpg" title="" class="width-content" >}}
{{< img src="mycelium-straw-3-week3-03.jpg" title="" class="width-content" >}}
