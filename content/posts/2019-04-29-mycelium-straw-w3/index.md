---
projects: ["Mycelium, straw and weaving"]
title:  "Week 3"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-week3-02.jpg" title="Dish 2 : Layered Straw" class="width-small" >}}

{{< img src="mycelium-straw-week3-02b.jpg" title="Dish 2 : Layered Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week3-03.jpg" title="Dish 3 : Woven Straw" class="width-small" >}}

{{< img src="mycelium-straw-week3-03b.jpg" title="Dish 3 : Woven Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week3-04.jpg" title="Dish 4 : Straw Husk" class="width-small" >}}

{{< img src="mycelium-straw-week3-05.jpg" title="Dish 5 : Coconut Fibre" class="width-small" >}}

{{< img src="mycelium-straw-week3-05b.jpg" title="Dish 5 : Coconut Fibre (back)" class="width-small" >}}

{{< img src="mycelium-straw-week3-06.jpg" title="Box 6 : Beehive Style" class="width-small" >}}

Dishes 2, 3 and box 6 still growing well but after 3 weeks the mycelium growth is only what would be expected at 1 week.

Dishes 4 and 5 do still not grow, only in the back of the Dish 5 because we reused the petri dish where our mycelium mother grown and so now, the mycelium is growing into the coffee grounds that was left in the petri dish.
