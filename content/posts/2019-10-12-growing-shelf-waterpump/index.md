---
projects: ["Growth Shelf"]
title:  "Water Pump"
tags: ["Fablab Amsterdam", "Fablab Intern", "bioponic system", "3D printing", "DC motor", "hacklab"]
---

I have the planter shelf with the bell siphon to ebb away the water from the top to the bottom. I now need a way to transport the water from the water tank (bottom) to the planter pot (top). I have planned to use a waterpump and I decided to make my own one as we have a lot of DC motors here in the Fablab.

{{< img src="waterpump-01.jpg" title="" class="width-content" >}}

I decided to use one of the small DC motor in the middle of the picture above. The ref wrote on it is FK260SA14280 - KD23603 (made in China). It seems to be a Mabushi 12V DC motor 8600RPM. I decided to use this one because the moving pin is in the center of the motor, it is small and there is only a + and - connection.

Below are the links of the instructions I consulted to understand how to make my own water pump.

- [DIY 3V waterpump from Instructables](https://www.instructables.com/id/DIY-water-pump-1/)
- [DIY 12V waterpump from Youtube](https://www.youtube.com/watch?v=ym4DIPBwQjs)
- [DIY 12V waterpump with 3D printed pieces from Youtube](https://www.youtube.com/watch?v=2nQBoQ3HucM)
- [DIY waterpump from Instructables](https://www.instructables.com/id/diy-small-water-pump/)


{{< img src="waterpump-02.jpg" title="" class="width-content" >}}


{{< img src="waterpump-03.jpg" title="" class="width-small" >}}
{{< img src="waterpump-04.jpg" title="" class="width-small" >}}


{{< img src="waterpump-05.jpg" title="" class="width-small" >}}
{{< img src="waterpump-09.jpg" title="" class="width-small" >}}

{{< img src="waterpump-06.jpg" title="" class="width-small" >}}
{{< img src="waterpump-07.jpg" title="" class="width-small" >}}
{{< img src="waterpump-08.jpg" title="" class="width-small" >}}
