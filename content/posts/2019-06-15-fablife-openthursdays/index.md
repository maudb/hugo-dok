---
projects: ["FabLife"]
title:  "Open Thursdays"
tags: ["Fablab Amsterdam", "Fablab Intern", "open day", "lasercutting", "3D printing", "3D scanning", "CNC milling", "vinyl cutting"]
---

Each Thursday from 12:00 to 17:00, the Fablab is open to the public. During the open Thursdays we explain how the machines work, what you can make with them and what the philosophy is behind the maker movement and digital fabrication.

Critical makers with interesting projects are always welcome. We are happy to answer any questions or helping find your way in the Amsterdam maker scene, as there is a growing number of so-called 'Makerplace' at the public libraries (OBA) or in the city where you can make things with digital fabrication.

We also hosted some teenagers for one or few days for internship in company accompanied by their high school.

- With them, we did custom boxes with the lasercutter by using [makercase website](https://www.makercase.com/).
- We did 3D scan of their faces and print them in 3D.
- We did stickers.

{{< img src="openday-01.jpg" title="" class="width-small" >}}

{{< img src="openday-03.jpg" title="" class="width-small" >}}

{{< img src="openday-02.jpg" title="" class="width-small" >}}

{{< img src="openday-04.jpg" title="" class="width-small" >}}

{{< img src="openday-07.jpg" title="" class="width-small" >}}

{{< img src="openday-06.jpg" title="" class="width-small" >}}

{{< img src="openday-05.jpg" title="" class="width-small" >}}

{{< img src="openday-08.jpg" title="" class="width-small" >}}



We also host some interesting projects from people who wants to make things in a fablab way like those two persons:

- [Debby](https://www.dmarchena.eu/) who is a partially sighted woman, wanted to make her own perfectly fit night mask because she is very sensitive to day light during morning. <br>
Then we 3D scanned her face with the 3D sense scanner we have here, and we milled her face in foam with the CNC.

{{< img src="openday-09.jpg" title="" class="width-small" >}}

{{< img src="openday-10.jpg" title="" class="width-small" >}}

{{< img src="openday-11.jpg" title="" class="width-small" >}}

{{< img src="openday-12.jpg" title="" class="width-small" >}}

- [Dan](http://buzzo.com/) who is an artist, designer and researcher, wanted to make the [Open Source Beehives project](http://greenfablab.org/opensourcebeehives/) from the Green Fablab of Barcelona. All the instructions are online, we helped him to use the CNC and make his own one to take part of the project.

{{< img src="openday-13.jpg" title="" class="width-small" >}}

{{< img src="openday-14.JPG" title="" class="width-small" >}}

{{< img src="openday-15.jpg" title="" class="width-small" >}}
