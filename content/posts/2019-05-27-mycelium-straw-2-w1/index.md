---
projects: ["Mycelium, straw and weaving"]
title:  "week 1"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

The level of growth obtained after 1 week with the millet spawn is much better than the previous test.

The woven tests were surrounded by a mix of straw scarps and grain spawn in an attempt to counteract the fact that much of the grain spawn fell out of the twisted samples during preparation, just like in the previous test.

We also made samples of “randomly” ordered straw with coffee, flour and nothing added for comparison to the woven tests. Unfortunately, the boxes with flour and coffee were contaminated with some bad green inside even though we close the boxes with parafilm around the lid and made some holes on the top of the lid closed with pieces of wadding to ensure air renewal while keeping a sterile environment.

{{< img src="mycelium-straw-2-week1-02.jpg" title="woven twisted and tied with coffee ground" class="width-small" >}}

{{< img src="mycelium-straw-2-week1-01.jpg" title="woven twisted and tied with flour" class="width-small" >}}

{{< img src="mycelium-straw-2-week1-04.jpg" title="woven twisted and tied with nothing" class="width-small" >}}

{{< img src="mycelium-straw-2-week1-03.jpg" title="random straw with nothing" class="width-small" >}}

{{< img src="mycelium-straw-2-week1-05.jpg" title="wood dust with nothing" class="width-small" >}}

{{< img src="mycelium-straw-2-week1-06.jpg" title="" class="width-small" >}}
