---
projects: ["FabLife"]
title:  "Learned and Goals"
tags: ["Fablab Amsterdam", "Fablab Intern", "personal notes"]

---

### 1. Learn project development and management into a Fablab

    - managing all machines and softwares at the fablab
    - assist visitors or Waag employees with software and machining
    - contribute to a safe and pleasant work environment
    - assist with cleaning, maintaining and improving the lab
    - contribute to open source documentation by documenting and sharing the development of my own project
    - Do tour during the Open Thursdays

### 2. Learn modeling with new 2D and 3D software

Software: Inkscape; FreeCAD; Fusion360; MeshMixer

### 3. Learn 3D printing and scanning

Hardware: Prusa i3 MK3/MMU2S; 3D sense scanner

Software: Cura; PrusaSlicer

#### 4. Learn CNC machining in a small and large scale

Hardware: Roland Modela MDX-20 small milling machine; Shopbot CNC big milling machine

Software: Mods online; Vcarve

### 5. Learn lasercutting and vinylcutting

Hardware: BRM lasers CO2 + software included; VinylCutter Roland GX-24

Software: Slic3r for Fusion360

### 6. Learn mould design, construction and casting


### 7. Learn website development tools

Hugo : static website generator

Atom : markdown editor

### 8. Learn version controls protocols

Git versioning

### 9. Learn design and programming circuit boards


### 10. Learn the use of sensors and output devices

Arduino

### 11. Learn interpret and implement programming protocols

Arduino tutorials

### 12. Learn mechanical and machine design

Waterpump, Bioponic shelf

### 13. Learn integration of techniques into a final projects
