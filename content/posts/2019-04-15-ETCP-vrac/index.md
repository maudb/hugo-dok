---
projects: ["ETC Project"]
title:  "Vrac"
tags: ["furniture", "modular system", "OpenStructures", "Fablab Amsterdam"]
---

{{< img src="ETCP-vrac-01.jpg" title="assembly system, test with a strap" class="width-small" >}}

{{< img src="ETCP-vrac-02.jpg" title="crash test of the 3D printed rings (yellow)" class="width-small" >}}

{{< img src="ETCP-vrac-03.jpg" title="The 60cmx60cm Open Structure Grid engraved on a textile with the lasercuter" class="width-small" >}}

{{< img src="ETCP-vrac-04.jpg" title="detail of the Open Structure Grid engraved on a textile with the lasercuter" class="width-small" >}}

{{< img src="ETCP-vrac-07.jpg" title="assembly test, non right angle with meltable bio-plastic" class="width-small" >}}

{{< img src="ETCP-vrac-05.jpg" title="assembly test, non right angle with 3D printed pieces and meltable bio-plastic" class="width-small" >}}

{{< img src="ETCP-vrac-06.jpg" title="assembly system, test with bolds and nuts" class="width-small" >}}
