---
projects: ["Mycelium, straw and weaving"]
title:  "Week 4"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

Laura, an other intern at Waag from Germany, is trying to make mycelium leather following [this method from the BioFarmForum](https://biofabforum.org/t/method-of-making-mycelium-leather/218).

Because she came back to Germany for one month, she asked us to give some food to her mycelium.

{{< img src="mycelium-straw-week4-00.jpg" title="" class="width-full" >}}

We took this opportunity to put some food in our petri dishes aswell to boost the growth.

The nutrient-rich liquid is composed by :  

- Malt extract (3g)
- Yeast extract (3g)
- Peptone (5g)
- Glucose (10g)
- Distilled water (1000L)

and sterilized by autoclaving

{{< img src="mycelium-straw-week4-01.jpg" title="" class="width-small" >}}

{{< img src="mycelium-straw-week4-02.jpg" title="" class="width-small" >}}

{{< img src="mycelium-straw-week4-03.jpg" title="" class="width-small" >}}

{{< img src="mycelium-straw-week4-04.jpg" title="" class="width-small" >}}

The funny fact is, as you can see above, the mycelium is waterproof and the nutrient liquid doesn't go inside the composition. But we figure out this fact by putting the nutrient liquid where there is less mycelium. After this inoculation food giving process, we close the dishes and box with parafilm and reput them into the foam box in the boiler room, like before.  

{{< img src="mycelium-straw-week4-12.jpg" title="Dish 2 : Layered Straw" class="width-small" >}}

{{< img src="mycelium-straw-week4-13.jpg" title="Dish 3 : Woven Straw" class="width-small" >}}

{{< img src="mycelium-straw-week4-13b.jpg" title="Dish 3 : Woven Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week4-14.jpg" title="Dish 4 : Straw Husk" class="width-small" >}}

{{< img src="mycelium-straw-week4-15.jpg" title="Dish 5 : Coconut Fibre" class="width-small" >}}

{{< img src="mycelium-straw-week4-15b.jpg" title="Dish 5 : Coconut Fibre (back)" class="width-small" >}}

{{< img src="mycelium-straw-week4-16.jpg" title="Box 6 : Beehive Style" class="width-small" >}}
