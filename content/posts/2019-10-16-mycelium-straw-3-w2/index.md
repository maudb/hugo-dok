---
projects: ["Mycelium, straw and weaving"]
title:  "week 2"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-3-week2-01.jpg" title="" class="width-content" >}}
{{< img src="mycelium-straw-3-week2-02.jpg" title="" class="width-content" >}}
{{< img src="mycelium-straw-3-week2-03.jpg" title="" class="width-content" >}}
