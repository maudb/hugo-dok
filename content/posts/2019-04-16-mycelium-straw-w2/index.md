---
projects: ["Mycelium, straw and weaving"]
title:  "Week 2 "
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-week2-01.jpg" title="Dish 1 : Random Straw" class="width-small" >}}

{{< img src="mycelium-straw-week2-02.jpg" title="Dish 2 : Layered Straw" class="width-small" >}}

{{< img src="mycelium-straw-week2-02b.jpg" title="Dish 2 : Layered Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week2-03.jpg" title="Dish 3 : Woven Straw" class="width-small" >}}

{{< img src="mycelium-straw-week2-03b.jpg" title="Dish 3 : Woven Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week2-04.jpg" title="Dish 4 : Straw Husk" class="width-small" >}}

{{< img src="mycelium-straw-week2-05.jpg" title="Dish 5 : Coconut Fibre" class="width-small" >}}

{{< img src="mycelium-straw-week2-06.jpg" title="Box 6 : Beehive Style" class="width-small" >}}

We found some green in Dish 1, the dish is contaminated because there was an hole in the parafilm, we decide to remove this dish from the box.

Dishes 2, 3 and Box 6 look good, the mycelium is growing. And next time, we'll put the twisted straws (beehive style) in a best fit box to force the mycelium to grow on the straw and not around.

The mycelium in Dish 4 and Dish 5 is not growing, the straw is definitely too dry.
