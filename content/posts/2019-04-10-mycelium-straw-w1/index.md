---
projects: ["Mycelium, straw and weaving"]
title:  "Week 1 "
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-week1-01.jpg" title="Dish 1 : Random Straw" class="width-small" >}}

{{< img src="mycelium-straw-week1-02.jpg" title="Dish 2 : Layered Straw" class="width-small" >}}

{{< img src="mycelium-straw-week1-02b.jpg" title="Dish 2 : Layered Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week1-03.jpg" title="Dish 3 : Woven Straw" class="width-small" >}}

{{< img src="mycelium-straw-week1-03b.jpg" title="Dish 3 : Woven Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week1-04.jpg" title="Dish 4 : Straw Husk" class="width-small" >}}

{{< img src="mycelium-straw-week1-05.jpg" title="Dish 5 : Coconut Fibre" class="width-small" >}}

{{< img src="mycelium-straw-week1-06.jpg" title="Box 6 : Beehive Style" class="width-small" >}}

The mycelium we had in the lab was not a strong specimen and growth was slower than expected. The samples of the woven (Dish 3) and layered (Dish 2) tests showed the most growth.

Most of the mycelium we used to inoculate the twisted sample (Box 6 - beehive style) fell out while actually twisting and tying the small sample. Thus, there was a limited amount feeding on the straw itself.

The driest samples (Dishes 4 and 5) showed almost no growth.
