---
projects: ["Mycelium, straw and weaving"]
title:  "Week 4"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-3-week4-01.jpg" title="tiny mushroom are coming on the right" class="width-content" >}}
{{< img src="mycelium-straw-3-week4-02.jpg" title="" class="width-content" >}}

I thought the mycelium grew well inside the structure but when I removed the plastic film, I saw that actually the mycelium grew only around the structure, between the straw and the plastic film and not inside the structure. It could be because I did the structure very compact then there is not enough air inside the structure for the mycelium growth.

{{< img src="mycelium-straw-3-week4-05.jpg" title="" class="width-content" >}}

So the idea to fill a bag with the structure of straw and plenty of cut straw to avoid holes doesn't work like I did. A better way to do it could be to had an intermediate step between the first inoculation and second
