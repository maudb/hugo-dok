---
projects: ["Mycelium, straw and weaving"]
title:  "week 1"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-3-week1-01.jpg" title="" class="width-content" >}}
{{< img src="mycelium-straw-3-week1-02.jpg" title="" class="width-content" >}}
{{< img src="mycelium-straw-3-week1-03.jpg" title="" class="width-content" >}}
