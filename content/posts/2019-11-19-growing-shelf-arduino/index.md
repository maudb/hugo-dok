---
projects: ["Growth Shelf"]
title:  "Arduino"
tags: ["Fablab Amsterdam", "Fablab Intern", "electronics", "bioponic system"]
---


Like I said, this project is mostly an exercise to design with electronics. Then, I chose to start with those three entities:

- a solar powering system
- a water pump
- an arduino as a timer

I keep the door open to lately add some sensors (temperature, light, moisture, ..) and some LED.

{{< img src="emma-01.jpg" title="" class="width-small" >}}
{{< img src="emma-02.jpg" title="" class="width-small" >}}

to complete

<!-- Henk gave me a solar panel with a charging board that he had in his special closet, I didn't find so much instructions about it and it made burn the board  -->
