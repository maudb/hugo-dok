---
projects: ["Mycelium, straw and weaving"]
title:  "Week 3"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

We dried it in the oven during 45 min at 200°C ([like last time]({{< relref "2019-05-13-mycelium-straw-w6" >}})).

{{< img src="mycelium-straw-2-kill-01.jpg" title="front" >}}

{{< img src="mycelium-straw-2-kill-02.jpg" title="back" >}}

The result is interesting because it worked like a glue to attached straw together and then it makes the structure stronger but the problem is still that the mycelium when it is dried is very crumbly and so when the structure of straw and mycelium is not strong enough or when there is more mycelium than straw, those parts break too easily.

I think we should try two things:

- grow mycelium and the woven twisted straw in a bag and fill all the holes with little pieces of mycelium, then we gonna have a very compact bag of straw in where mycelium can grow and glue it
- try to soak the composition in glycerine before to dry it, like the [mycelium leather process](https://biofabforum.org/t/method-of-making-mycelium-leather/218), to make the mycelium less crumbly and more stretchy.
