---
projects: ["Mycelium, straw and weaving"]
title:  "Week 6"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

{{< img src="mycelium-straw-week6-02.jpg" title="Dish 2 : Layered Straw" class="width-small" >}}

{{< img src="mycelium-straw-week6-02b.jpg" title="Dish 2 : Layered Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week6-03.jpg" title="Dish 3 : Woven Straw" class="width-small" >}}

{{< img src="mycelium-straw-week6-03b.jpg" title="Dish 3 : Woven Straw (back)" class="width-small" >}}

{{< img src="mycelium-straw-week6-04.jpg" title="Dish 4 : Straw Husk" class="width-small" >}}

{{< img src="mycelium-straw-week6-04b.jpg" title="Dish 4 : Straw Husk (back)" class="width-small" >}}

{{< img src="mycelium-straw-week6-05.jpg" title="Dish 5 : Coconut Fibre" class="width-small" >}}

{{< img src="mycelium-straw-week6-05b.jpg" title="Dish 5 : Coconut Fibre (back)" class="width-small" >}}

{{< img src="mycelium-straw-week6-06.jpg" title="Box 6 : Behive Style" class="width-small" >}}

Dishes 2, 3 and Box 6 are continued to grow but we don't know if it's due to the food.
But the food helped Dish 4 and killed Dish 5. I think we put too much in Dish 5.

Anyway, because we read that 6 weeks is the longest time for this oyster mycelium growing before mushrooms appear, we decided it's time to stop the growing process and dry the mycelium and finally test the resistance of those compositions.

{{< img src="mycelium-straw-week6-07.jpg" title="" class="width-content" >}}

{{< img src="mycelium-straw-week6-08.jpg" title="" class="width-content" >}}

So we put the content of Dish 2, Dish 3, Dish 4 and Box 6 in the oven during 45 min at 200°C.

{{< img src="mycelium-straw-week6-09.jpg" title="After drying, everything looks a bit stronger." class="width-small" >}}

{{< img src="mycelium-straw-week6-10.jpg" title="But the different layers of straws don't stick together." class="width-small" >}}

{{< img src="mycelium-straw-week6-16.jpg" title="The dried layered straw is a failure, the mycelium didn't grow between the different layers and the composition is not strong enough to allow the mycelium to glue the different elements together. But you can see that it still worked a little, only it's very brittle" class="width-small" >}}

{{< img src="mycelium-straw-week6-12.jpg" title="The dried woven straw is not so bad but it breaks very easily aswell, maybe because it's only one layer and so it's very fragile." class="width-small" >}}

{{< img src="mycelium-straw-week6-14.jpg" title="dried woven straw" class="width-small" >}}

{{< img src="mycelium-straw-week6-15.jpg" title="dried woven straw" class="width-small" >}}

{{< img src="mycelium-straw-week6-20.jpg" title="dried woven straw" class="width-small" >}}

{{< img src="mycelium-straw-week6-13.jpg" title="The dried beehive style is the most promising !" class="width-small" >}}

{{< img src="mycelium-straw-week6-17.jpg" title="Certainly because the composition was stronger at the beginning." class="width-small" >}}

{{< img src="mycelium-straw-week6-18.jpg" title="But also because the mycelium grew inside the straw and glue everything together." class="width-small" >}}

The result of the Husk straw was so anecdotic to talk about it.

## What we learned in this first test :

- We need a very strong composition of straw at the beginning to hope have a stronger composition with mycelium. In most of the tests the mycelium grew mostly on the surface of the straw, only adding superficial strength. Only the tiwsted and tied “ skep bee- hive” style test survived handling.
- Mycelium needs a humid environment to grow so it's better to wet the straw before doing the composition.
- try a different way to dry the result to keep it more flexible and less brittle. Maybe by following [this method from the BioFarmForum](https://biofabforum.org/t/method-of-making-mycelium-leather/218)
- time to try something bigger and thicker through the twisted test was by far the best, we will use this sample in a new run of tests with a stronger, faster growing mycelium specimen.
