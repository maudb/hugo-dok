---
projects: ["Mycelium, straw and weaving"]
title:  "week 2"
tags: ["Wetlab Amsterdam", "biodesign", "Fablab Intern"]
---

We stopped the test after week 2.

The leaders in terms of overall mycelium growth were the samples to which wheatflour was added to the straw before inoculating. The woven samples felt stronger than the unordered ones upon initial examination, in support of the hypothesis that strength in the substrate material (twisting and then weaving) is passed on to the resulting mycelium composite. The extra sccraps of straw scattered to fill in the gaps in the woven straw samples seem vital to the end strength, long, woven fibres are not enough by themselves.

{{< img src="mycelium-straw-2-week2-02.jpg" title="woven twisted and tied with flour" class="width-small" >}}

{{< img src="mycelium-straw-2-week2-01.jpg" title="woven twisted and tied with flour" class="width-small" >}}

{{< img src="mycelium-straw-2-week2-04.jpg" title="random straw with nothing" class="width-small" >}}

{{< img src="mycelium-straw-2-week2-03.jpg" title="random straw with nothing" class="width-small" >}}

{{< img src="mycelium-straw-2-week2-05.jpg" title="wood dust with nothing" class="width-small" >}}

All of the test samples were pressed with a book press and then left to grow for another 5 days, in line with the process described by Phillip Ross in his patent for making strong bricks of mycelium material. This pressing is said to add strength and facilitate faster growth.

{{< img src="mycelium-straw-2-week2-06.jpg" title="" class="width-small" >}}

{{< img src="mycelium-straw-2-week2-07.jpg" title="" class="width-small" >}}

 You can see a test piece using wood dust as a substrate as it appears after pressing. The wood dust test was done as a curiosity but showed decent strength. It could possibly be used to fill in the gaps around the woven straw style samples for more strength.

Next step is to dry them.
