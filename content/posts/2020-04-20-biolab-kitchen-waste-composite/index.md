---
projects: ["BioLab Kitchen"]
title:  "Waste Composite"
tags: ["waste design", "circular economy", "home lab"]
---

I have been spending a bit of my CO-VID19 quarantine's time on designing with food waste in order to explore "creative food cycles" and to find interesting properties of different materials.

The recipes are from [materiom website](materiom.org/), I will complete soon all the details of my journey.

## Coffee Grounds composite

### CoffeeCup

{{< img src="coffeeCup-05.jpg" title="" class="width-small" >}}
{{< img src="coffeeCup-04.jpg" title="" class="width-small" >}}
{{< img src="coffeeCup-03.jpg" title="" class="width-small" >}}
{{< img src="coffeeCup-02.jpg" title="" class="width-small" >}}
{{< img src="coffeeCup-01.jpg" title="" class="width-small" >}}
{{< img src="coffeeCup-00.jpg" title="" class="width-small" >}}

### CoffeeBowl

{{< img src="coffeeBowl-07.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-06.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-05.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-04.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-03.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-02.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-01.jpg" title="" class="width-small" >}}
{{< img src="coffeeBowl-00.jpg" title="" class="width-small" >}}

## Orange Peel composite

{{< img src="orange-01.jpg" title="" class="width-small" >}}
{{< img src="orange-02.jpg" title="" class="width-small" >}}
{{< img src="orange-03.jpg" title="" class="width-small" >}}
{{< img src="orange-04.jpg" title="" class="width-small" >}}

## Eggs Sheel composite

{{< img src="moldPlate-01.jpg" title="" class="width-small" >}}
{{< img src="eggPlate-00.jpg" title="" class="width-small" >}}
{{< img src="eggPlate-01.jpg" title="" class="width-small" >}}

## Black Tea composite

{{< img src="teaCup-01.jpg" title="" class="width-small" >}}
{{< img src="teaCup-02.jpg" title="" class="width-small" >}}
{{< img src="teaPlate-01.jpg" title="" class="width-small" >}}

## Conclusion

I found some trouble with the drying part, it takes quite a long time (up to 5-10 days) and the shape tends to change during this process. It also becomes relatively smaller. I tried to let them into the mold a bit longer but then, molds appear and it would take too long to dry. It could be interesting to try to oven bake them, but i'm afraid that the shape will change too much and cracks, or to design a mold  with holes for drying.
