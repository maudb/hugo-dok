---
projects: ["Grow Mycelium"]
title:  "TextileLab contribution"
tags: ["Wetlab Amsterdam", "TextileLab Amsterdam", "biodesign", "Fablab Intern"]
---

Find below my contribution to the TextileLab biomaterial library of Waag:

## Mycelium Leather

[→ get the .pdf file](https://www.dropbox.com/s/7ir2uk6yanwfy7o/VEGAN%20LEATHER%20mycelium.pdf?dl=0)

{{< img src="mycelium-textilelab01.jpg" title="" class="width-content" >}}


### Test 1

For steps 1-2-3, read my post [Mycelium growth test - making]({{< relref "2019-10-09-Grow-Mycelium-making" >}}) to know how to inoculate some mycelium into the liquid nutrient medium.

{{< img src="mycelium-leather1-00.jpg" title="the result after 10 days of incubation (step 4)" class="width-small" >}}

{{< img src="mycelium-leather1-01.jpg" title="the result after 10 days of incubation (step 4)" class="width-small" >}}

{{< img src="mycelium-leather1-03.jpg" title="Harvest the mycelium sheet (step 5)" class="width-small" >}}

{{< img src="mycelium-leather1-02.jpg" title="Plasticise the mycelium and let it soak for several hours (step 6)" class="width-small" >}}

{{< img src="mycelium-leather1-04.jpg" title="the result of the dried mycelium sheet after several hours in the oven (step 7)" class="width-small" >}}

{{< img src="mycelium-leather1-05.jpg" title="the result of the dried mycelium sheet after several hours in the oven (step 7)" class="width-small" >}}

### Test 2

{{< img src="mycelium-leather2-01.jpg" title="The result after 10 days of incubation" class="width-small" >}}

{{< img src="mycelium-leather2-02.jpg" title="The result after 20 days of incubation. I wanted to wait a bit more and hoped to have it more dense but it did the opposite, it started to reduce. I guess not enough food anymore and so it slowly started to die." class="width-small" >}}

{{< img src="mycelium-leather2-03.jpg" title="Anyway, I still plasticised and dried the mycelium sheet and the result is still fine but very thin." class="width-small" >}}

{{< img src="mycelium-leather2-04.jpg" title="I also did a try with some mycelium inoculated in dog food because the medium and the mycelium were really compacted together and the result is interesting because it is thicker than the other tests. " class="width-small" >}}

## Mycelium and Straw

[→ get the .pdf file](https://www.dropbox.com/s/smvwmsoy1t5xj63/COMPOSITE%20mycelium%20and%20straw.pdf?dl=0)

Read my project "Create a Composite Material With Mycelium and Straw" to see the process.

{{< img src="mycelium-textilelab02.jpg" title="" class="width-content" >}}
