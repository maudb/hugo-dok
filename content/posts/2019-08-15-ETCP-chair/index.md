---
projects: ["ETC Project"]
title:  "Make a Chair"
tags: ["furniture", "modular system", "OpenStructures", "Fablab Amsterdam"]
---
{{< img src="ETCP-chair-15.jpg" title="" class="width-content" >}}
{{< img src="ETCP-chair-14.jpg" title="" class="width-content" >}}
{{< img src="ETCP-chair-12.JPG" title="" class="width-small" >}}
{{< img src="ETCP-chair-13.JPG" title="" class="width-small" >}}


The idea was to build something which needs to be strong to test the solidity of the [system]({{< relref "2019-01-01-etcp-intro" >}}), so I made an ETCP Chair.

{{< img src="ETCP-chair-11.jpg" title="" class="width-content" >}}
{{< img src="ETCP-chair-08.jpg" title="" class="width-small" >}}
{{< img src="ETCP-chair-09.jpg" title="" class="width-small" >}}

I used different kind of assembling to test the solidity

{{< img src="ETCP-chair-07.jpg" title="old inside tyre to make the seat" class="width-small" >}}
{{< img src="ETCP-chair-01.jpg" title="bolds and nuts system" class="width-small" >}}
{{< img src="ETCP-chair-02.jpg" title="ropes system" class="width-small" >}}
{{< img src="ETCP-chair-03.jpg" title="3D printed system" class="width-small" >}}
{{< img src="ETCP-chair-04.jpg" title="3D printed system upgraded" class="width-small" >}}
{{< img src="ETCP-chair-05.jpg" title="3D printed system upgraded" class="width-small" >}}
{{< img src="ETCP-chair-06.jpg" title="3D printed system upgraded" class="width-small" >}}
