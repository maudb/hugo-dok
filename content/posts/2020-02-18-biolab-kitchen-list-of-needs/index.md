---
projects: ["BioLab Kitchen"]
title:  "List of needs"
tags: ["biodesign", "biohack", "home lab"]
---

First things first:

## BioSafety Level 1 Rules

- work with well-characterized agents which do not cause disease in healthy humans
- wash hands upon entering and exiting the lab
- Research with these agents may be performed on standard open laboratory benches without the use of special containment equipment
- eating and drinking are prohibited in laboratory areas
- Potentially infectious material must be decontaminated before disposal, either by adding a chemical such as bleach or isopropanol or by packaging for decontamination elsewhere (autoclave)
- Personal protective equipment is only required for circumstances where personnel might be exposed to hazardous material
- BSL-1 laboratories must have a door which can be locked to limit access to the lab. However, it is not necessary for BSL-1 labs to be isolated from the general building

To know more about it :

- [BioSafety Levels](https://en.wikipedia.org/wiki/Biosafety_level)
- [BioSafety Rules](https://en.wikipedia.org/wiki/Biosafety) vs [BioSecurity Rules](https://en.wikipedia.org/wiki/Biosecurity)

## Additional Lab Safety Rules

- hair pulled back
- wear closed shoes
- wear lab coat
- (wear googles)
- (wear gloves)
- labeling everything (name, date, content)
- seal everything with parafilm
- store them in the appropriate emplacement
- clean everything after use


## Where to find stuff

- [Pidiscat](https://www.pidiscat.cat/ca/): supplier in Barcelona (Poblenou)
- [Quimics Dalmau](https://quimicsdalmauonline.com/): supplier in Barcelona (Eixample)
- find a second hand stock
- you can also find most of the things on an online shop to make your own cosmetic like [Gran Velada](https://www.granvelada.com/es/) in Catalunya or [Mi Cosmetica Casera](https://www.micosmeticacasera.es/) in Spain (South-West)


## What we need

MATERIALS |WORKSPACE |STERILISE |GROW      |STORE
----------|----------|----------|----------|-----
pans, glass bottles, petri dishes, flasks, beakers, funnel, lab scale, thermometer, latex potholder,  small kitchen knife, scissors, spoons, tweezer, pipettes, inoculation loop spreader, parafilm, labels, parmanent pen|easy-to-clean work bench, a clean-box to work, gaz burner, ethanol 70%, wash bottle, paper towel, bain marie, oven, disinfectant gel, lab coat, safety rules displayed, access to water|pressure cooker, hot plate, autoclave bags and tape|incubator|sealed boxes, fridge, freezer|

<!--
  MATERIALS:
  - pans, glass bottle, petri dishes, flasks, beakers, funnel, lab scale, thermometer, latex potholder, spoons, small kitchen knifes, scissors, tweezers, pipettes, inoculation loop spreader, etc
  - Parafilm, label, permanent pen
  WORKSPACE:
  - easy-to-clean work benches
  - a clean transparent box to work
  - gaz burner
  - ethanol 70% and a wash bottle
  - paper towel
  - bain-marie or microwave
  - oven
  - disinfectant gel
  - lab coat
  - safety rules displayed
  - access to water
  STERILISE:
  - Autoclave : Pressure cooker + hot plate
  - autoclave bags and tape
  GROW:
  - Incubator (DIY one or reptilium)
  STORE:
  - sealed boxes to store all the stuff
  - fridge (could be a sealed box into the main fridge)
  - freezer (same)
-->


## Links

### BioHacking:

- [BioHack Academy github](https://biohackacademy.github.io/)
- [BioFabForum](https://biofabforum.org/) Bio Fabrication recipes and advices
- [BioFabForum: Links by topic](https://biofabforum.org/t/links-by-topic-in-one-document/215)
- [GreenLab Blog](https://www.greenlab.org/blog/) London-based
- [DIY Bio](https://diybio.org/) Community of DIY biologists

### Bio-materials and bio-techniques:

- [Materiom](https://materiom.org/) Nature's Recipe online Book
- [Algae Lab](https://algae-lab.com/) GB supplier
- [BioShades](https://bioshades.bio/) Textile Dyeing with bacteria
- [Textile Academy wiki](https://class.textile-academy.org/)

### Biodesigners / Bioartists:

- [Magical contamination](https://magical-contamination.tumblr.com/)
- [Jannis](http://www.jannis.world/) designer, material researcher, and a lot more
- [Plasticula](https://www.plasticula.com) Turning plastic into compostable material
- [Circology](https://www.circology.org/) Material research and circular design studio
- [BioBabes](https://www.biobabes.co.uk/) Feminist collective of biodesigners, makers, and biohackers
- [Quimera Rosa](https://quimerarosa.net/) Nomadic lab that researches and experiments on body, technoscience and identities
- [dezeen's Biofabrication posts](https://www.dezeen.com/tag/biofabrication/)
- [Loes Bogers (Textile Academy 2020) ](https://class.textile-academy.org/2020/loes.bogers/projects/archiving_new_naturals/) "Archiving new naturals"
- [Bela Rofe (Textile Academy 2020)](https://class.textile-academy.org/2020/bela.rofe/projects/GAIA/) "An interwoven tapestry of algae and female human hair"
- [Beatriz Sandini (Textile Academy 2020)](https://class.textile-academy.org/2020/beatriz.sandini/projects/0-final-project/) "The Ephemeral Fashion Lab"
- [MaDE (Material Designers)](http://materialdesigners.org/) is competition, event series and platform devoted to realising the positive impact material designers can have across all creative sectors.
- [Faber Futures](https://faberfutures.com/) London-based award-winning futures agency operating at the intersection of nature, design, technology and society
- [Ginko Bioworks](https://www.ginkgobioworks.com/) Boston-based biotech company founded in 2009 by scientists from MIT
- [Creative Food Cycles](https://creativefoodcycles.org/) explores new way of Food-Art-Creativity
- [Mychrome Valentina Dipietro]()
- [Organic Matters](https://www.instagram.com/_organicmatters/) connecting local producers, material designers and industry to create compostable and regenerative applications
- [Journey to the Microcosmos](https://www.youtube.com/channel/UCBbnbBWJtwsf0jLGUwX5Q3g/videos) youtube channel
- [STARTS Prize 2020](https://starts-prize.aec.at/index.html) Science + Technology + Arts
- [Blast Studio](https://www.blast-studio.com/) 3D printed mycelium in UK

### Make your own Laminar Flow:

- [Building an open source laminar flow cabinet](https://waaglfc.tumblr.com/)

### Make your own incubator:

- [BioHack Academy class 3](https://biohackacademy.github.io/bha6/class/3/pdf/3.4%20Incubator%20design.pdf) (incubator)
- [BioHack Academy Student's Incubator](https://juneyong-lee.github.io/Incubator/)
- [easier version](https://biodesign.cc/2013/12/25/diy-incubator/) with a bulb, a temp sensor and a control board.

## Schedule

1. List of needs and costs
2. First equipment: incubator
3. Second equipment: a sterile workplace
4. Dedicate and arrange a place for the BioLab Kitchen
5. Play and upgrade little by little
