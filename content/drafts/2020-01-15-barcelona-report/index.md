---
projects:
title:  "Barcelona"
tags: ["notes"]
draft: true
---

## Barcelona Digital City (https://ajuntament.barcelona.cat/digital/en)

- Barcelona as a laboratory of innovation: Read the [Relat d’Innovació Municipal Mandat 2015-2019 - Ajuntament de Barcelona](https://ajuntament.barcelona.cat/digital/sites/default/files/2019_05_relat_dinnovacio_ponencia2015_2019.pdf) and discover all the emergent projects led by the municipality.


## Fablab Network

- [FabLab Poblenou (from Iaac)](https://fablabbcn.org/)
- [Valldaura Labs (from IaaC)](https://valldaura.net/)
- [Green Fablab Valldaura (from Iaac)](http://greenfablab.org/)
<!-- sent; internship proposal -->
- [FabCity Hub (supported by Iaac)](https://bcn.fab.city/)
<!-- sent; internship accepted-->
- [Made Barcelona](http://made-bcn.org/) in Sants. Open Doors each Monday from 18:00, Arduino Playground each Tuesday from 18:00, etc
<!-- sent: community of members, no job option but sometimes collaboration payed for members -->
- [Soko Tech](https://soko.tech) in La Sagrera is a studio working on STEAM.
<!-- sent -->
- [FabLab Sant Cugat](http://fablabsantcugat.com/) it is out of the city, on the other side of the Collserola
<!-- sent + itw -->
- [Fab Casa Del Mig / Punt Multimedia](https://www.puntmultimedia.org/) Sants - Parc de l'Espanya Industrial
<!-- sent -->
- [Colectic](http://colectic.coop/) Technologia per la transformacio social
- [LaSalle](https://lasalle.cat/) educational institution with campus all over Catalunya


## [Ateneus de Fabricacio](https://ajuntament.barcelona.cat/ateneusdefabricacio/es/)

- Ateneu de Fabricació Fabra i Coats
- Ateneu de Fabricació Ciutat Meridiana
- Ateneu de Fabricació Fàbrica del Sol
- Ateneu de Fabricació Parc Tecnològic
- Ateneu de Fabricació Gràcia
- Ateneu de Fabricació de les Corts
<!-- sent -->


## Barcelona Maker Faire participants

- [OSBH](https://www.osbeehives.com) Pairing the timeless knowledge of beekeepers and a global database of bee sound
- [ROMI](https://romi-project.eu/) Open Tools for farming communities
- [3D Seed](http://3d-seed.com/) Fablab/MakerSpace in Cerdanyola del Vallès
<!-- TO SEND? -->
- [Made Makerspace Barcelona](http://made-bcn.org/) in Sants
<!-- sent: community of members, no job option but sometimes collaboration payed for members -->
- [Intervencio B Murals](https://www.bmurals.com/) Urban Art Center in La Sagrera
- [Collective Edibles](https://collective-edibles.eu/) Emily FCH personal project
- Plastic For Good
- [El Barri Circular](https://www.instagram.com/circularbarris/) is a [siscode](https://siscodeproject.eu/), Fablab and H2020 project in Poblenou. This is about exploring, prototyping, debating new ideas of circular economy.
- [Make Works](https://make.works/catalonia/) is an open access directory of fabricators, material suppliers, workshops and manufacturers.
- [Pop-Machina](https://pop-machina.eu/) Pop-Machina is a H2020 project that seeks to highlight and reinforce the links between the maker movement and circular economy in order to promote environmental sustainability and generate socio-economic benefits in European cities.
- [PhabLabs 4.0](http://www.phablabs.eu/) combining the World of Photonics with the growing creative ecosystem of existing Fab Labs.
- Espai Challange
- [UpnaLab](https://upnalab.com/) Combining computer science, physics, electronics, mechanics and chemistry to create interactive devices.
<!-- TO SEND? but very out of barcelona -->
- [OpenStreetMap Catalunya](https://upnalab.com/)
- [OSA Electronics](https://www.osaelectronics.com/) Shop & Learn
- [Portrait Painter Robot](https://hackaday.io/project/19468-portrait-painter)
- [Elisava Circular](https://www.elisava.net/) Barcelona School of Design and Engineering
- [Honext Material](https://honextmaterial.com/) High-end reclaimed material for circular living, all made from cellulose waste
<!-- sent: let's meet after estado de alarma -->
- [Cesire](https://agora.xtec.cat/cesire/) Center for Educational Resources Specific to Support Innovation and Educational Research
- [Phone Farm(ing)](http://www.phonefarm.eu/) Computing & Database Network
- [ICIQ](http://www.iciq.org/) Institut Catala d'Investigacio Quimica
- [IEEC Nanosatellites](http://www.ieec.cat/en/home) Institut d’Estudis Espacials de Catalunya
- [Cyborg Foundation](https://www.cyborgfoundation.com/) is an online platform for the research, development and promotion of projects related to the creation of new senses and perceptions by applying technology to the human body.
- [La Hora Maker](https://lahoramaker.com/) blog & podcasts
- 3D Print Barcelona
- HelloHealth
- [FabLab Sant Cugat](http://fablabsantcugat.com/)
<!-- sent + itw -->
- [Rural Lab](https://www.ruralab.org/) "What Could Happen If We Combined Digital Fabrication Technology With Real Rural Needs"
- [TransfoLab BCN](https://www.transfolabbcn.com/home) center for trash investigation
<!-- sent: let's meet after estado de alarma -->
- [Formbytes](https://www.formbytes.com/) open source kit for 3D printing
- EduCATbot
- [IRI Social Robotics](https://www.iri.upc.edu/research/mobile_robotics)
- exposició de robotica i làmpades
- [Colectic](http://colectic.coop/) Technologia per la transformacio social
- [EASD Llotja](http://www.llotja.cat/llotja/p/1/737/0/Inici) Escola d'Art i Superior de Disseny
- [Space exploration at school](https://sese.asu.edu/)
- [Hi-Storia](http://www.hi-storia.it/code-and-print/) Platform to code and print your 3D design
- [Mochi Robot](https://learnwithmochi.com/ Mochi teaches children coding in a playful, hands-on way as they explore the universe and learn colors, numbers, letters, basic words and more!
- [Mini Makers](https://www.mini-makers.com/) STEAM-based learning program dedicated to providing a unique environment in which students have a well-rounded, beyond-the-box classroom experience.
- [Parconier](https://parconier.cc/) motivate young people to learn, through the use of educational robotics kits, which can be adapted to different environments.
- [Esaac Baix Cost](http://www.esaac.es/baixcostcat/) Jornades de Productes de Suport de Tecnologia Lliure
- mechanical clot auto tuner
- Rover 004
- [Vektorelle Giulia](https://vektorelle.com/giulia-cc) Giulia is a downloadable and 3d printable learning set for visually impaired persons of all ages
- [Fabkulele](https://fabacademy.org/2019/labs/barcelona/students/josep-marti/FinalProject.html) final fab academy project by Josep Marti
- RE-naif
- [Tangencial](http://tangencial.es/index.html) learn making
<!-- TO SEND -->
- [FabTextiles](https://fabtextiles.org/) Experimental Digital Open Source Couture. BioShades Workshop, Textile Bacteria Dyeing. TCBL
- [Back to Eco](https://backtoeco.com/en/) home denim collection
- [IAOSAS](https://iaios.com/pages/sobre-iaios) pullovers, 100% recycled and recyclable
- [Intexter](https://www.upc.edu/intexter/en) Institute of Textile Research and Industrial Cooperation of Terrassa. INTEXTER
- ReGen-e.rar
- [Computer Vision Centre (CVC)](http://www.cvc.uab.es/) non profit institute, leader in research and development in the field of computer vision
<!-- TO DIG -->
- Arrow
- Prusa
- Ajutament de Barcelona
- [Privacy Seed](https://privacy-seed.org/) Exploring intersections of human identity and its digital extensions
- [Lifebox](https://2019.makerfairerome.eu/it/eventi/?ids=131) a biology related interactive simulator and also an artistic installation
- [TOCHAK balance bike EV](http://tochak.ba/) lightweight wooden balancing bicycle carefully designed for children form age two to five. It is adaptable to child growth and 100% recyclable.
- The Puerta Project


## Technologies Center, 3D printing & others

- [Noumena](https://noumena.io/) Noumena is a multidisciplinary tech company in Eixample implementing innovative solutions in the fields of Robotics, 3D printing and Wearable Tech.
<!-- sent: internship proposal -->
- [wasp3D](https://wasp3d.noumena.io/) Curated by Noumena, this is a space specialized in 3D Printing Service, Consulting, Design and technical support in the center of Barcelona.
- [La Maquina](http://www.lamaquina.io/) Big scale 3D printing
- [FabCafe Barcelona](https://fabcafe.com/es/barcelona/)
<!-- sent -->
- [Lab ONE](https://terreform.com/one-lab/) summer program to explore design with living matter
- [NeedLab](http://www.needlab.org/) non-profit design and research organization aimed at solving sustainability issues for communities using human-centered design strategies
<!-- TO SEND -->
- [COACT Lab](https://www.coactlab.org/) An environmental tech accelerator & innovation hub
in the hills of Barcelona
- [TMTMTM @Hangar](https://hangar.org/en/residents/collective-residents/tmtmtm/)
- [MedioDesign](https://www.mediodesign.com/)
<!-- sent -->
- [Atta33](https://atta33.com/)
<!-- sent -->
- [Leitat](https://www.leitat.org/) Tech Foundation to create and transfer Social value
<!-- TO SEND -->
- [Eurecat](https://eurecat.org/) Centro Tecnológico de Catalunya
<!-- TO DIG -->
- [IBEC](https://www.ibecbarcelona.eu/) Institute for bioengineering of Cat
<!-- TO DIG -->
- [TresD Nou](http://tresdenou.com/es/) 3D printing in PobleNou
- [entresd](https://entresd.es/es/) 3D printer brand
- [Foodini](https://www.naturalmachines.com/) food printer
- [3D Factory Incubator](https://www.dfactorybcn.org/) The High Technology Business Incubator for 3D Printing, helping industries in digital transformation
- [HP 3D printing Barcelona](https://press.hp.com/us/en/press-kits/2019/barcelona-3d-printing-and-digital-manufacturing-center-of-excellence.html) Barcelona 3D Printing and Digital Manufacturing Center of Excellence
- [BCN3D](https://www.bcn3d.com/) 3D printer brand

## Green Labs

- [Green City Lab](https://www.greencitylab.org/)
- [ConnectHort](https://connecthort.org/)
- [Espai Germanetes](https://straddle3.net/en/proyectos/germanetes)

## Social Labs

- [Decibim Barcelona](https://www.decidim.barcelona/)
- [Barcelona Actua](https://www.barcelonactua.org/)

## EU Projects in Barcelona

- [IaaC EU Projects](https://iaac.net/project-category/eu-projects/)
- [ReFlow](https://reflowproject.eu/) Co-creating circular and regenerative resource flows in cities
- [Distributed Design](https://distributeddesign.eu/) Networking Hub for the European Design Market
- [Grow Observatory](https://growobservatory.org/) Citizen science for climate action
- Romi
- Make Works
- Pop Machina
- Siscode
- Foodshift


## Architecture in bcn

- [LACOL](http://www.lacol.coop/) cooperative of architects
- [Straddle3](https://straddle3.net/en/) bottom-up architecture with an open-source philosophy
<!-- TO DIG -->
- [dezeen's barcelona article](https://www.dezeen.com/?s=barcelona&hPP=40&idx=vetg_livesearchable_posts&p=0&fR[post_type_label][0]=&is_v=1)
<!-- TO DIG -->
Raul Sanchez Architect
<!-- SENT -->
https://mhap.studio
https://arquitecturag.wordpress.com/
<!-- SENT -->
http://cru.cat/
https://cargocollective.com
http://mas-aqui.com/
https://thehallstudio.com/
https://www.manupages.es/
http://www.nookarchitects.com/


## Design in bcn

- [Berta Julià Sala](https://bertajuliasala.com/)
- [Oiko](http://www.oikodesignoffice.com/)
- [Verdllimona](https://fr.verdllimona.cat/) carpenter
- [Elsa Casanova](https://elsacasanova.com/)


## Espacios Cultural

- [La Escosa](https://laescocesa.org/)
- [Hangar](https://hangar.org)
- [Can Batlló](https://www.canbatllo.org/)


## Coworking

- [La Vaca](https://www.lavacacoworking.com/) Poble Sec
- [Valkiria](https://valkiriahubspace.com) Poble Nou
- [Coworking BCN](www.coworkingbarcelona.es) Gloriès
- [A Poc A Poc - Nest City Lab](www.apocapocbcn.com) Poble Nou
<!-- SENT -->
- [Pasaje Montoya](http://pasajemontoya.com/) Poble Nou
- [BippHub](https://www.bipphub.org/) aims to promote and accelerate the impact of organisations and professionals working in Barcelona on the Sustainable Development Goals (SDGs) and global challenges.


## Books

- [FabCity - The mass distribution of (almost) everything](https://issuu.com/iaac/docs/fabcitymassdistribution)


## Shops

- [Servei Estació](www.serveiestacio.com) Hardware Megastore
- [Diotronic](diotronic.com) electronic store


## TO DIG

- [Poblenou Urban District](https://www.poblenouurbandistrict.com/)
- [Iaac Projects](https://iaac.net/project/)
