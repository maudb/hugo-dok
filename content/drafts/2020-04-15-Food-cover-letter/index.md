---
projects: ["Food Process"]
title:  "Food industry Cover Letter"
tags: ["application"]
draft: true
---

Through my education and professional practice, I have always wanted to shape the future and promote the principles of a circular and sustainable economy.

I graduated in interior architecture (visual and space art) in 2014 at ESA St-Luc Brussels and I have worked in a Brussels based architecture office for 4 years where I did housing and building renovation and extension, from design to site supervision. I have explored the possibilities of doing something new by keeping the old (inspired by the wabi-sabi and kintsugi Japanese’s concepts) and the possibilities of restoring old constructive materials to service with the principle of a circular economy. Those experiences have sharpened my sense of creativity as well as my sense of management and responsibilities through technical challenges, customer and supplier relations and team working.

In January 2019, I decided to open my horizon by integrating the Waag Fablab in Amsterdam where I have developed my interest in open design, digital fabrication and open hardware. I have seen the maker movement as a way to create and explore (almost) everything and to give the possibility to anyone to learn by doing and to erase the borders between disciplines. Indeed, at Waag, I also had the chance to spend a part of my time in their Open WetLab which is a place for bio-art, biodesign and do-it-together biology. I have learned there how to work with living things and I have been doing some researches by growing mycelium into different medium in order to find new biocomposite materials to design bio-based pieces.

I am now continuing my researches with biohacking and mycelium growing here in Barcelona with the FabCity Hub which is working to understand, learn and experiment with projects aiming for urban self-sufficiency within food, energy and materials. Together, we are working to build The BioLab Kitchen. The aim of the project is to create a “BioHack Set Up” that anybody can easily build and use at home in order to give the possibility to everybody to reimagine all aspects of food, waste and living systems for an emerging bio-circular economy and for a bio-based future. It is about going from an extractive economy to a regenerative economy and it is questioning the way of producing and the mass-production model in which we are. As we want to contribute in the change, we are working to keep all that information accessible and open source as working within a community shows better and faster results and seems to be a key to resolve emergency problems.

At the same time, at home, I began to explore the process of fermenting food and drink. As I have turned vegan one year ago, I got closer to a macrobiotic diet. I thrive on making tempeh, sourdough bread, kombucha, kefir and many other small fermented food productions. I am also playing the game to grow some of my vegetables. I took advantage of my time at Waag to build my own bioponic system and I am learning urban farming and ecological interactions thanks to the FabCity Hub. I see sustainable food as a challenge and a power for the next future and as a target that materialises and influences ongoing deep changes in society and technology.

It makes me question which is the role that designers can have to strengthen the sustainability of food?

Indeed, cross-disciplinary collaboration and creativity supported by scientific research only expand, powered by global imperatives such as the urgency to develop and implement cleaner technologies and the rise of do-it-yourself biology. In my opinion, this convergence of fields, as well as the expert with the amateur, is necessary to support the ongoing effort to go against the negative impacts of the legacies of the Industrial Revolution. It will lead until the reconception of growth, sustainability and the primary design principles which is value generation. I think that the challenge of changing the story is immense, and it obliges the "users" of art and design to engage themselves in one of the fields usually reserved for science, and the "users" of science to allow themselves to be imbued with things that they do not usually master.

It is in this perspective that I want to give my expertise and my vision to XXX as a New Food Product Developer. I want to take part in the change by challenging the food industry with new ways of consuming. As the mycelium and the mushroom kingdom are a too little known and a too little exploited field and the macrobiotic a key of health balance.

I’m looking forward to discussing this further with you.

Kind regards,

Maud Bausier



<!-- biology as the most distributed technology
the next revolution will be the biotechnology revolution

Ever since humans have been designing, we've mimicked those who do it best. And who's the master of design? Nature, of course. Now we're not only replicating nature but using it as a co-creator.
Biology and design have proved a perfect match for creating innovative solutions.

the mushroom kingdom to save the world helping by all the micro organism whom surounded us.

If green and local growth has become a major leitmotiv of contemporary society in terms of nutritional and health issues, the biodesign aims to empower designers to assume a more proactive attitude, regarding food as a cultural vehicle of identity, innovation and social integration. -->
