# Maud Bausier

Bonjour, I am a Belgian interior architect /designer currently based in Barcelona. I am passionate about new ways of designing, about how low and high tech can meet, about natural process and living materials, about waste and circular economy. I see design as a social change.

Here you will find all the documentation of my current projects. I have started this website during my time at Waag’s Fablab of Amsterdam in January 2019 and I continue to use it as an online tool to keep a track of my experiences (fails and process included) but also to share.

Feel free to dig on it, to [contact me](mailto:maudb@pm.me) or to know more about me through my [CV & Folio](https://www.dropbox.com/s/z6cj7cybxp51kb2/MaudBausier_2020.pdf?dl=0)
