---
 title: "FabLife"
 cover: "/images/openday-01.jpg"
---

During one year from January 2019, I integrated the Waag Fablab of Amsterdam. There, I developped my interest in open design, digital fabrication and open hardware. I learned how to work in a Fablab, use and manage its machines. I supported ongoing projects and the Open Thursday program but I also had access to tools, machines and support to develop my own projects and produce prototypes in the environment of Waag which aims to make technology & society more open, fair and inclusive.

Here you'll find overview of projects I have been busy with.  

For more details about Amsterdam and my Waag's Journey, check [Amsterdam Report]()
