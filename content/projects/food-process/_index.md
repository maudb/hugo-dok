---
 title: "Food Process"
 cover: "/images/food-cover.jpg"
---

Through my education and my professional practice, I always wanted to shape the future and promote the principles of a sustainable economy. When I understood that my alimentation and my food consumption are certainly the best way to act and to promote ecological and social changes, I decided to buy fair, organic and waste-free as most as I can. For the same reasons, I also turned to a vegan diet.

At home, I began to explore the process of fermenting food and drinks as I learned about the benefits of a macrobiotic diet. And it's a lot of fun! I thrive on making tempeh, sourdough bread, kombucha, kefir and many other small fermented food productions. It's a real culture and you have to take care of your new bacteries-friends like with pets.

I am also playing the game to grow some of my vegetables and mushrooms. I took advantage of my time at Waag to build my own bioponic system, to learn about mycelium and the kingdom of mushrooms, and I am learning urban farming and ecological interactions thanks to the FabCity Hub. I see sustainable food as a challenge and a power for the next future and as a target that materialises and influences ongoing deep changes in society and technology.

Indeed, everything is connected! We have seen that green and local growth has become a major leitmotiv of contemporary society in terms of nutritional and health issues, in the same logic the biodesign (the practice of designing with biology) aims to empower designers to assume a more proactive attitude, regarding food as a cultural vehicle of identity, innovation and social integration.
