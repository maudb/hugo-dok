---
title: "Print version"
---

## Maud Bausier (BE) - 6 months into a FabLab

This is my documentation about what I made during my time at Waag Fablab Amsterdam. It's all about experimentation, fail and process.

In the same idea, this .pdf is an export of my documentation website made with an very experimental "html to print" process. Please excuse in advance any errors in layout.


### Legend

✎Posts ☼Projects ✿Categories ⸫Tags

The followings pages will show series of Posts reunited by Projects

### Introduction about myself

I graduated as an interior architect in 2014 at ESA Brussels, I first explored the movements of social design, open design and new forms of designing during my studies, research and internship. I then worked for 4 years in a small architecture office for private clients to do housing renovation and housing extensions. I learned how to build and had worksite experience and I explored the possibilities of doing something new by keeping the old and the possibilities of putting back into use some old constructive materials with the principle of a circular economy. Then I started an internship of 6 months into the Waag Fablab Amsterdam where I learned digital fabrication and every other way of making and prototyping.

### Contact

Maud Bausier <br>
Provooststraat 55 <br>
1050 Brussels

+32 479 43 46 40 <br>
maud.b@pm.me
